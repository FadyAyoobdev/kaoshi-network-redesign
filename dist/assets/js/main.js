$(document).ready(() => {
  const addOverlay = () => {
    const overlay = document.createElement('div');
    overlay.classList.add('overlay');
    overlay.innerHTML = `
        <i class="feather icon-play text-secondary"></i>
    `;

    return overlay;
  };

  const toggleSteps = () => {
    const steps = document.querySelectorAll('.steps .d-flex');
    const stepsArr = [...steps];
    const stepImages = document.querySelectorAll('.step-image img');

    // get the index of the current active element
    const index = Number(
      stepsArr
        .map((key, i) => key.classList.contains('active') && String(i))
        .filter((key) => key)[0]
    );

    // remove current active
    stepImages[index].style.display = 'none';
    steps[index].classList.remove('active');

    if (index < 3) {
      steps[index + 1].classList.add('active');
      stepImages[index + 1].style.display = 'block';
    } else {
      steps[0].classList.add('active');
      stepImages[0].style.display = 'block';
    }
  };

  const navbarInit = () => {
    const navbar = document.querySelector('nav.fixed-top');
    const header = document.querySelector('header');
    const floatingBtn = document.querySelector('.floating-btn');

    if (window.scrollY > header.offsetTop + header.offsetHeight) {
      floatingBtn.style.display = 'block';
    } else {
      floatingBtn.style.display = 'none';
    }

    if (window.scrollY > 10) {
      navbar.classList.add('bg-primary', 'shadow');
    } else {
      navbar.classList.remove('bg-primary', 'shadow');
    }
  };
  // FAQ.html Nav Bar background
  const faqNav = () => {
    const faqNavBar = document.querySelector('.faq-fixed-top');
    console.log(faqNavBar);

    if (window.scrollY > 10) {
      faqNavBar.classList.add('bg-primary', 'shadow');
    } else {
      faqNavBar.classList.remove('bg-primary', 'shadow');
    }
  };

  window.addEventListener('scroll', faqNav);

  faqNav();

  $('#slider')
    .not('.slick-initialized')
    .slick({
      dots: true,
      centerMode: true,
      infinite: true,
      centerPadding: '60px',
      slidesToShow: 3,
      autoplay: true,
      autoplaySpeed: 2000,
      prevArrow: $('.prevSlide'),
      nextArrow: $('.nextSlide'),
      responsive: [
        {
          breakpoint: 1224,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 1024,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 600,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 1,
          },
        },
      ],
    });

  document.querySelectorAll('.video').forEach((elem) => {
    const overlay = addOverlay();
    elem.appendChild(overlay);
  });

  $('.video').click(function () {
    const video = $(this).children('video')[0];

    if (video.paused) {
      video.play();
      $(this).children('.overlay')[0].remove();
    } else {
      const overlay = addOverlay();
      video.pause();
      $(this).append(overlay);
    }
  });

  window.addEventListener('scroll', navbarInit);

  $('.floating-btn').click(() => {
    $('html, body').animate({ scrollTop: 0 }, 'slow');
  });

  navbarInit();

  // Toggle get started steps
  setInterval(toggleSteps, 3000);

  // handle links with @href started with '#' only
  $(document).on('click', 'a[href^="#"]', function (e) {
    // target element id
    var id = $(this).attr('href');

    // target element
    var $id = $(id);
    if ($id.length === 0) {
      return;
    }

    // prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault();

    // top position relative to the document
    var pos = $id.offset().top - 50;

    // animated top scrolling
    $('body, html').animate({ scrollTop: pos });
  });
});

//////////////////////////////////////
///Typing Effect
class TypeWriter {
  constructor(txtElement, words, wait = 3000) {
    this.txtElement = txtElement;
    this.words = words;
    this.txt = "";
    this.wordIndex = 0;
    this.wait = parseInt(wait, 10);
    this.type();
    this.isDeleting = false;
  }

  type() {
    //Current index of word
    const current = this.wordIndex % this.words.length;
    //Get full text of current word
    const fullTxt = this.words[current];

    // Check if deleting
    if (this.isDeleting) {
      //remove char
      this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
      // Add char
      this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    //Insert txt into element
    this.txtElement.innerHTML = `<span class="txt">${this.txt}</span>`;

    //Initial Type Speed
    let typeSpeed = 300;

    if (this.isDeleting) {
      typeSpeed /= 2;
    }

    //If word is complete
    if (!this.isDeleting && this.txt === fullTxt) {
      typeSpeed = this.wait;
      //Set delete to true;
      this.isDeleting = true;
    } else if (this.isDeleting && this.txt === "") {
      this.isDeleting = false;
      //Move to next word
      this.wordIndex++;
      // Pause before start typing
      typeSpeed = 500;
    }

    setTimeout(() => this.type(), 500);
  }
}

// Init On DOM Load
document.addEventListener("DOMContentLoaded", init);

// Init App
function init() {
  const txtElement = document.querySelector(".txt-type");
  const words = JSON.parse(txtElement.getAttribute("data-words"));
  const wait = txtElement.getAttribute("data-wait");
  //Init TypeWriter
  new TypeWriter(txtElement, words, wait);
}
